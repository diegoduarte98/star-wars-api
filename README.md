# API Star Wars
```
mvn clean install && mvn spring-boot:run
ou  
docker-compose up --build
```

## Tecnologias envolvidas

O Projeto foi desenvolvido com:
* Java 1.8
* Maven
* Spring Boot
* Spring Data JPA
* MySql
* Swagger
