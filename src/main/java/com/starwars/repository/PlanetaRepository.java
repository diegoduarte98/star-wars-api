package com.starwars.repository;

import com.starwars.model.Planeta;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlanetaRepository extends JpaRepository<Planeta, Long> {

    Page<Planeta> findByNome(String nome, Pageable paginacao);
}
