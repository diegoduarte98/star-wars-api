package com.starwars.service;

import com.starwars.facade.SwapiDTO;
import com.starwars.model.Planeta;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface PlanetaService {

    Page<Planeta> listar(String nome, Pageable paginacao);
    
    Page<Planeta> listar(Pageable paginacao);

    Planeta cadastrar(Planeta planeta);

    Optional<SwapiDTO> obterPorFiltro(String search);

    Optional<Planeta> detalhar(Long id);

    void remover(Long id);
}
