package com.starwars.service.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import com.starwars.facade.SwapiDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.starwars.facade.SwapiFacade;
import com.starwars.model.Planeta;
import com.starwars.repository.PlanetaRepository;
import com.starwars.service.PlanetaService;

@Service
public class PlanetaServiceImpl implements PlanetaService {

	private PlanetaRepository planetaRepository;

	private SwapiFacade swapiFacade;

	public PlanetaServiceImpl(PlanetaRepository planetaRepository, SwapiFacade swapiFacade) {
		this.planetaRepository = planetaRepository;
		this.swapiFacade = swapiFacade;
	}

	@Override
	public Page<Planeta> listar(String nome, Pageable paginacao) {
		return planetaRepository.findByNome(nome, paginacao);
	}

	@Override
	public Page<Planeta> listar(Pageable paginacao) {
		return planetaRepository.findAll(paginacao);
	}
	
	@Override
	@Transactional
	public Planeta cadastrar(final Planeta planeta) {
		return planetaRepository.save(planeta);
	}

	@Override
	public Optional<Planeta> detalhar(Long id) {
		return planetaRepository.findById(id);
	}

	@Override
	public void remover(Long id) {
		planetaRepository.deleteById(id);
	}

	@Override
	public Optional<SwapiDTO> obterPorFiltro(String search) {
		return swapiFacade.obterPorFiltro(search);
	}
}
