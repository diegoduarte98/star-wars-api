package com.starwars.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("star-wars-api")
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.starwars.controller"))
				.paths(PathSelectors.any())
				.build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("star-wars-api")
				.description("Star Wars Api")
				.termsOfServiceUrl("http://www.apache.org/licenses/LICENSE-2.0")
				.contact(new Contact("Diego Duarte", "https://github.com/diegoduarte98", "diegoduarte98@gmail.com"))
				.license("JApache 2.0")
				.licenseUrl("http://www.apache.org/licenses/LICENSE-2.0")
				.version("V1.0.0").build();
	}
	
}
