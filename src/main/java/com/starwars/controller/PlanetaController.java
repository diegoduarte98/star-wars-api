package com.starwars.controller;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import com.starwars.facade.SwapiDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.starwars.controller.dto.PlanetaDTO;
import com.starwars.controller.form.PlanetaForm;
import com.starwars.model.Planeta;
import com.starwars.service.PlanetaService;

@RestController
@RequestMapping("/planetas")
public class PlanetaController {

	private PlanetaService planetaService;

	private PlanetaController(PlanetaService planetaService) {
		this.planetaService = planetaService;
	}

	@GetMapping
	public Page<PlanetaDTO> listar(@RequestParam(required = false) String nome, @PageableDefault(size = 3, sort = "id", direction = Sort.Direction.DESC) Pageable paginacao) {

		if(nome == null) {
			Page<Planeta> planetas = planetaService.listar(paginacao);
			return PlanetaDTO.converter(planetas);
		} else {
			Page<Planeta> planetas = planetaService.listar(nome, paginacao);
			return PlanetaDTO.converter(planetas);
		}
	}
	
	@GetMapping("externos")
	public ResponseEntity<SwapiDTO> obterPorFiltro(@RequestParam("nome") String nome) {
		Optional<SwapiDTO> optionalSwapi = planetaService.obterPorFiltro(nome);
		return ResponseEntity.ok(optionalSwapi.get());
	}

	@GetMapping("/{id}")
	public ResponseEntity<PlanetaDTO> detalhar(@PathVariable Long id) {
		Optional<Planeta> optional = planetaService.detalhar(id);
		return ResponseEntity.ok(new PlanetaDTO(optional.get()));
	}
	
	@PostMapping
	public ResponseEntity<PlanetaDTO> cadastrar(@Valid @RequestBody PlanetaForm form, UriComponentsBuilder builder) {
		Planeta planeta = planetaService.cadastrar(form.converter(planetaService));

		URI uri = builder.path("/planetas/{id}").buildAndExpand(planeta.getId()).toUri();
		return ResponseEntity.created(uri).body(new PlanetaDTO(planeta));
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long id) {
		planetaService.remover(id);
	}
}
