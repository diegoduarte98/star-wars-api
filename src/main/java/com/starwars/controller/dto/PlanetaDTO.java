package com.starwars.controller.dto;

import com.starwars.model.Planeta;
import io.swagger.annotations.ApiModel;
import lombok.*;
import org.springframework.data.domain.Page;

@Getter
@Setter
@Builder
@ApiModel("Planeta")
@AllArgsConstructor
@NoArgsConstructor
public class PlanetaDTO {

    private Long id;

    private String nome;

    private String clima;

    private String terreno;

    private Integer quantidadeAparicoes;

    public PlanetaDTO(Planeta planeta) {
        this.id = planeta.getId();
        this.nome = planeta.getNome();
        this.clima = planeta.getClima();
        this.terreno = planeta.getTerreno();
        this.quantidadeAparicoes = planeta.getQuantidadeAparicoes();
    }

    public static Page<PlanetaDTO> converter(Page<Planeta> planetas) {
        return planetas.map(PlanetaDTO::new);
    }
}
