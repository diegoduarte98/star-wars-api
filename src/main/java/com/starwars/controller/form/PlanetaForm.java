package com.starwars.controller.form;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.starwars.facade.SwapiDTO;
import com.starwars.model.Planeta;
import com.starwars.service.PlanetaService;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PlanetaForm {

	@NotNull
    private String nome;

	@NotNull
    private String clima;

	@NotNull
    private String terreno;
    
    @JsonIgnore
    private Integer quantidadeAparicoes;

    public Planeta converter(PlanetaService planetaService) {
        return Planeta.builder()
                .nome(this.nome)
                .clima(this.clima)
                .terreno(this.terreno)
                .quantidadeAparicoes(planetaService.obterPorFiltro(nome).map(SwapiDTO::getQuantityAppearances).orElse(0))
                .build();
    }
}
