package com.starwars.facade;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SwapiDTO {

    private String name;

    private Integer quantityAppearances;

    public SwapiDTO(Planets planets) {
        this.name = planets.getName();
        this.quantityAppearances = planets.getFilms().size();
    }
}
