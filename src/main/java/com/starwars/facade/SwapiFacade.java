package com.starwars.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import lombok.AllArgsConstructor;

import java.util.Optional;

@Component
public class SwapiFacade {

	@Value("${urls.swapi}")
	private String url;

	@Autowired
	private RestTemplate restTemplate;

	public Optional<SwapiDTO> obterPorFiltro(String search) {

		SwapiResponse swapiResponse = restTemplate.getForObject(url + "/planets/?search=" + search, SwapiResponse.class);

		return swapiResponse.getResults().stream().map(SwapiDTO::new).findAny();
	}
}