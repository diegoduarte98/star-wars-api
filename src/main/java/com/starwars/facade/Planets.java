package com.starwars.facade;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Planets {

    private String name;

    private String terrain;
    
    private String climate;
    
    @Builder.Default
    private List<String> films = new ArrayList<>();
}
