package com.starwars.validator;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public class CustomError {
	
	@Getter
	private List<CustomErrorDetails> errors = new ArrayList<>();
	
	public void addFieldError(String field, String message) {
		CustomErrorDetails fieldError = new CustomErrorDetails(field, message);
		errors.add(fieldError);
    }
}
