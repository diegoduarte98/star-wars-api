package com.starwars.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Planeta {

	@Id
	@GeneratedValue
	private Long id;
	
	@NotNull
	private String nome;

	@NotNull
	private String clima;

	@NotNull
	private String terreno;

	@Setter
	@NotNull
	private Integer quantidadeAparicoes;
}
