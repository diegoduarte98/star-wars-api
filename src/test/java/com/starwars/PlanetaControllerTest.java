package com.starwars;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.starwars.model.Planeta;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PlanetaControllerTest extends ApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setUp() {

    }

    @Test
    public void deve_retornar_http_status_201_ao_salvar_planeta() throws Exception {

        Planeta planeta = new Planeta(null, "Alderaan", "temperate", "grasslands", 0);

        this.mockMvc.perform(
                post("/planetas")
                        .content(asJsonString(planeta))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.quantidade_aparicoes", is(2)));
    }

    @Test
    public void deve_retornar_http_status_404_ao_tentar_salvar_planeta_com_nome_nulo() throws Exception {

        Planeta planeta = new Planeta(null, null, "temperate", "grasslands", 0);

        this.mockMvc.perform(
                post("/planetas")
                        .content(asJsonString(planeta))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[*].message", hasItem("must not be null")));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
