package com.starwars;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.starwars.facade.SwapiFacade;
import com.starwars.model.Planeta;
import com.starwars.repository.PlanetaRepository;
import com.starwars.service.PlanetaService;
import com.starwars.service.impl.PlanetaServiceImpl;

@RunWith(SpringRunner.class)
public class PlanetaServiceTest {

	private PlanetaService planetaService;

	@MockBean
	private PlanetaRepository planetaRepository;

	@MockBean
	private SwapiFacade swapiFacade;

	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	private Long ID_PLANETA_VALIDO = 1L;
	private Long ID_PLANETA_INVALIDO = 10L;

	@Before
	public void setUp() {
		this.planetaService = new PlanetaServiceImpl(planetaRepository, swapiFacade);
	}

	@Test
	public void deve_salvar_planeta_no_repositorio() throws Exception {
		planetaService.cadastrar(any());

		verify(planetaRepository).save(any());
	}
	
	@Test
	public void deve_retornar_planeta_por_id() throws Exception {
		when(planetaRepository.findById(ID_PLANETA_VALIDO)).thenReturn(Optional.of(
				Planeta.builder().id(ID_PLANETA_VALIDO).nome("Terra").build()
			));
		
		Optional<Planeta> optional = planetaService.detalhar(ID_PLANETA_VALIDO);

		assertThat(optional.get().getId()).isEqualTo(ID_PLANETA_VALIDO);
		
		verify(planetaRepository).findById(ID_PLANETA_VALIDO);
	}
	
	@Test
	public void deve_retornar_planeta_nao_encontrado() throws Exception {
		when(planetaRepository.findById(ID_PLANETA_INVALIDO)).thenReturn(Optional.empty());
		
		Optional<Planeta> optional = planetaService.detalhar(ID_PLANETA_INVALIDO);

		assertThat(optional.isPresent()).isEqualTo(false);
		
		verify(planetaRepository).findById(ID_PLANETA_INVALIDO);
	}
}
